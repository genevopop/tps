# Genómica de Poblaciones Humanas y Enfermedades - 2017

## Guías de los prácticos

* [TP 1 - Introducción a Linux](TP1/TP1_ComandosLinux_2017.pdf)
* [TP 2A - Ensembl](TP2/TP2-A-ENSEMBL/TP2 - ENSEMBL.pdf)
* [TP 2B - Biomart](TP2/TP2-B-Biomart/TP2 - Biomart_2017.pdf)
* [TP 3 - Control y pre-procesamiento de datos de NGS](TP3/ejercicios_tp3_dd.pdf)
* [TP 4 - Introducción al Ensamblado Genómico](TP4/ejercicios_tp4_dd.pdf)
* [TP 5 - RNA-Seq y Expresión](TP5/guia_TP5_dd.pdf)
* [TP 6 - Datos de 1000 Genomas, Linux &amp; vcftools](TP6/TP6.md)
* [TP 7 - Populations Genetics practice by John Novembre](TP7/TP7.md)
* [TP - Selection Browser](TP_Selection_Browser.pdf)
* [TP 8 - Selection Signals in 1000 Genomes data](TP8/TP8.md)
* [TP 9 - GWAS &amp; plink by Arcadi Navarro](TP9/TP9.md)
* [TP 10 - Variant Calling Pipeline](TP10/TP10.md)

## Instrucciones para correr la máquina virtual en su computadora

* Copie el archivo `Lubuntu_GPyEH_2017.vdi` en un pen drive o disco externo
  durante alguna de las clases. Pesa alrededor de 25 GB.
* Copie luego este archivo desde el pen a su computadora.
* Descargue en su computadora la aplicación de [Oracle Virtual Box](https://www.virtualbox.org/wiki/Downloads)
  que le permitirá ejecutar la máquina virtual. En la lista de arriba de 
  todo en esa página hay versiones para Windows, OS X (Mac) y Linux;
  descargue la que le corresponda.
* Instale la aplicación recién descargada en su computadora y ábrala.
* Cree una nueva máquina (cliquee el botón `New`) con la siguiente configuración:
    * Name and operating system:
        * Name: `Lubuntu GPyEH 2017`
        * Type: `Linux`
        * Version: `Ubuntu (64-bit)`
        * Cliquee `Next >`
    * Memory size:
        * Dependiendo de la cantidad de memoria RAM libre en su máquina,
          arrastre el cosito hasta un valor dentro de la barra verde,
          idealmente 4000 MB o más.
    * Hard disk:
        * Seleccione `Use an existing virual hard disk file`
        * Cliquee el ícono de la carpeta y elija el archivo `.vdi`
          que copio en su máquina.
          IMPORTANTE: **no** le conviene usar directamente el archivo desde
          el pen drive o disco externo, sino desde una copia en su disco rígido.
          La velocidad de lectura y escritura por USB es más lenta.
        * Cliquee `Create`
* Debería ver una nueva máquina llamada `Lubuntu GPyEH 2017` en la lista ahora.
  Cliquee esa opción y luego la flecha verde de `Start`.
* Es posible que la máquina arranque en una resolución baja por default.
  Mientras está corriendo, haga click derecho en el ícono del monitor que
  aparece abajo a la derecha.
  Le permitirá elegir `Virtual Screen 1` -> `Resize to 1920 x 1080`,
  u otra resolución que se vea bien en su monitor.
  Reinicie la máquina virtual (apagándola desde adentro de Lubuntu alcanza)
  y arránquela de nuevo: la resolución elegida debería aplicarse.
