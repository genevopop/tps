#!/bin/bash
ABYSS="/home/student/software/abyss-2.0.2/ABYSS/ABYSS"

for i in $(seq 29 2 47)
	do
		$ABYSS --kmer="$i" /home/student/TP4/reads/sra_data.fastq --out=/home/student/TP4/abyss/contigs-k"$i".fa
done
