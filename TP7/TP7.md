# TP 7 · Workshop: Population Genetics

John Novembre - November 12, 2013

Modified by Cristian Rohr, Juan Manuel Berros

## 0 - Preparation

```bash
cd ~/TP7
mkdir -p ~/TP7/results
```

## 1 - Dataset

To gain some experience running some basic population genetic analyses, we will look at Illumina 650Y array data from the CEPH-Human Genome Diversity Panel. This sample is a global-scale sampling of human diversity with 52 populations in total.
 
The data were generated at Stanford and are available at: http://hagsc.org/hgdp/files.html

Genotypes were filtered with a GenCall score cutoff of 0.25 and individuals with call rate &lt; 98.5% were removed. Beyond this, the set of individuals is fitered down to a set of 938 unrelated individuals
and then the data are provided in plink's binary format (as files `H938.bed`, `H938.fam`, `H938.bim`).

We will pay special attention to European populations -and some European isolates-
in this dataset: Orcadian (from Orkney islands, north of UK), Basques (north of Spain),
Sardinians (island to the west of Italy), and Adygei (south of Russia),
besides French and Italian people.

<div style="display: flex; align-items: flex-start;">
    <img src="imgs/orkney.jpg" width="350">
    <img src="imgs/sardinia.jpg" width="350">
    <img src="imgs/pais_vasco.png" width="350">
    <img src="imgs/adygea.jpg" width="350">
</div>

### 1.1 - Subset the data for European populations only

Get in the `data` directory to start working on the dataset:

```bash
cd ~/TP7/data
ls -lh
```

The `H938.clst.txt` file contains the specifications of each individual's population id.
Explore it with `less`:

```bash
less H938.clst.txt # clst stands for "cluster"

# Hit 'q' to quit when you're done
```
How many samples per population are there in the project? Use `awk`, `sort` and `uniq -c` to find this out:

```bash
awk '{ print $3 }' H938.clst.txt | sort | uniq -c | sort -nr -k1
```

Which population has the most samples and which population has the least samples in the HGDP?

Using an `awk` command we will make a list of all the individuals who are in the 6 Europan
populations of the HGDP (Sardinian, Basque, Italian, Adygei, Orcadian, French).
Using plink we can extract just those individuals into a new set of plink files.

First we keep individuals from these populations. Recall that `awk` will print the whole line if no `print` statement is included:

```bash
awk '$3 == "Sardinian" || $3 == "Basque" || $3 == "Italian" || $3 == "Adygei" || $3 == "Orcadian" || $3 == "French"' H938.clst.txt

# Now put this output in a file:

awk '$3 == "Sardinian" || $3 == "Basque" || $3 == "Italian" || $3 == "Adygei" || $3 == "Orcadian" || $3 == "French"' H938.clst.txt | tee Euro.clst.txt

# Explore the result: only the Euro populations should be kept
less Euro.clst.txt

# You can make sure this is the case with awk, sort and uniq
awk '{ print $3 }' Euro.clst.txt | sort | uniq -c

# Let's print *only* the population with most samples in the dataset:
awk '{ print $3 }' Euro.clst.txt | sort | uniq -c | sort -k1 | tail -n1

# Let's generate a PLINK dataset keeping only the Euro samples:
plink --bfile H938 --keep Euro.clst.txt --make-bed --out H938_Euro
```

**Question 1** - Explore the `Euro.clst.txt` file and the original `H938.clst.txt` file.
How many individuals are kept?

```bash
wc -l *clst*
```

If we know there are 938 individuals in the dataset, what's the 1.062 "total" that `wc` prints?

Does the number of sapmles in `Euro.clst.txt` match the number of samples kept
according to `plink`'s output?

```bash
grep "people" H938_Euro.log
```

**Question 2** - Explore the `H938_Euro.log` file.
How many SNPs did we have before and after choosing those six populations?

```bash
less H938_Euro.log
```

Plink output has a lot of information. Let's keep the lines that say either
"variants" or "people". `grep -E` allows you to use regular expressions
to filter the contents of a file. The expression `variants|people` will filter lines
that match any of those words, `variants` or `people`. Let's try it:

```bash
grep -E "variants|people" H938_Euro.log
```

#### Brief (kind of advanced) safety check using awk and paste

As an extra check and just for the sake of exercising our command line *skillz*,
let's compare the list of individuals in our source file, `Euro.clst.txt`, and in one 
of the output plink files, `H938_Euro.fam`, which keeps the sample IDs.

First, take a look at the `.fam` file to see how it's structured:

```bash
less H938_Euro.fam
```

There should be 6 columns in that file. I could tell you what each column
represents, but let's exercise the most basic skill of a bioinformatician: googling
stuff they don't know. Google "plink fam format" and try to find out what
the columns are from PLINK's documentation, which is quite good. Notice
we are using plink 1.9 (aka plink 2).

After this quick lookup, can you say what are the `-9`s in the last column
of the `.fam` file?

Since we don't care about the files that we will generate during this check,
we're going to put them in the temporary directory `/tmp`, which is periodically
emptied by Linux.

First, keep the column of sample names from each file:

```bash
awk '{ print $2 }' Euro.clst.txt | sort | tee /tmp/input_samples.list
awk '{ print $2 }' H938_Euro.fam | sort | tee /tmp/output_samples.list
```

`tee` allows us to simultaneously print to STDOUT (so we can see the result
in the screen) and write to a file. Check the temporary samples files were
created correctly: `ls -l /tmp/*samples*` and then `wc -l /tmp/*samples*`.

Now `paste` will allow us to put both lists side by side. Notice both
lists were `sort`ed in the previous step, so we should have two columns
of identical IDs side by side.

```bash
paste /tmp/input_samples.list /tmp/output_samples.list | tee /tmp/comparison
```

The last thing we need to do is checking that all lines match! Let's use `awk`'s `==` and `!=` operators to compare the values of both columns.

```bash
awk '$1 == $2 { print "They match :)" } $1 != $2 { print "They do not match :(" }' /tmp/comparison
```

Finally, we make sure all comparisons were correct:

```bash
awk '$1 == $2 { print "They match :)" } $1 != $2 { print "They do not match :(" }' /tmp/comparison | sort | uniq -c
```

Did you find 124 matches? Then we're good to go!

### 1.2 - Filter SNPs in linkage disequilibrium

We also need to prepare a version of each dataset in which we filter out sites that are in linkage disequilibrium using plink's pairwise genotypic LD filtering commands. First the whole `H938` group:

```bash
plink --bfile H938 --indep-pairwise 50 10 0.1

# The parameters of --indep-pairwise used here are:
#
# - check pairs of SNPs in 50 kb windows
# - advance in steps of 10 bases between windows
# - use an r^2 threshold of 0.1 to include/exclude SNPs

# Now we use the included SNPs to make a new dataset named "H938.LDprune"

plink --bfile H938 --extract plink.prune.in --make-bed --out H938.LDprune
```

Now we should have a new set of plink files under the prefix `H938.LDprune`.
Check this is so with `ls *LDprune*`.

**Question 3** - Explore the `plink.prune.in` and `plink.prune.out` files.
How many SNPs were kept and removed?

```bash
wc -l *prune.{in,out}

# Tip: in the command line, the curly braces {} expand the expression
# using each comma-separated option (here, "in" and "out").
#
# So this command is the same as:
#
# $ wc -l *prune.in *prune.out
```

We now repeat the protocol for the `H938_Euro` cluster.

```bash
plink --bfile H938_Euro --indep-pairwise 50 10 0.1

plink --bfile H938_Euro --extract plink.prune.in --make-bed --out H938_Euro.LDprune
```

**Question 4** - Explore the `plink.prune.in` and `plink.prune.out` files
(they were overwritten with the last commands).

How many SNPs were kept and removed in the `H938_Euro` subgroup?

## 2 - Exploring Hardy-Weinberg predictions

In this section, you will assess how well the genotypes at each SNP fit Hardy-Weinberg proportions
for these datasets of World vs. Euro samples.

Given the population structure in the datasets, we might have a chance to observe the Wahlund
effect, in which the observed propotion of heterozygotes is less than expected due to hidden population
structure (thought of another way, each sub-population is in a sense "inbred", lowering the
heterozygosity).

From now on, the commands assume that you are in the `results` subdirectory:

```bash
cd ~/TP7/results
ls -lh
```

### 2.1 - Using plink to get basic gentoype counts

To begin, run the `plink --hardy` to test departures from Hardy-Weinberg proportions.
To keep the analysis simple, use the `--chr` command and limit it to SNPs on chromosome 2.

Notice that you are not in the `data` directory anymore.
From now on, data files will be referenced with the relative
path `../data`, which means the `data` folder sibling of the current directory.

```bash
plink --bfile ../data/H938 --hardy --chr 2 --out H938
```

Explore the results of the HWE test:

```bash
head H938.hwe

# There seems to be one variant per line. Count them:

wc -l H938.hwe
```

Notice that, as Plink output notified, not all SNPs were tested: 53.652 out of 659.276
(because we only analysed chromosome 2 SNPs). Let's confirm this by counting
the number of chromosome 2 variants in plink's `.bim` file.

```bash
cat ../data/H938.bim

# Ok, chromosome seems to be the first column:

cat ../data/H938.bim | awk '$1 == "2"

# Let's count chromosome 2 variants:

cat ../data/H938.bim | awk '$1 == "2" | wc -l
```

Did the count yield 53.652?

Now let's investigate the rest of the file:

```bash
less H938.hwe
```

### 2.2 - Plotting in R

Now, we will use the following R function to make a plot of the frequency of each genotype relative
to its allele frequency (note: actually the code plots only a sampling of 3000 SNPs to avoid an
overloaded plot).

You can peek in the file to get a sense of what the plotting code looks like.

```bash
cat ../scripts/e2_2_ALL.R
```

Let's execute it now:

```bash
Rscript ../scripts/e2_2_ALL.R # For the whole dataset

ls -lh

# There should be a new PNG image with the plot now
# Open a file browser, browse to the TP7 results folder
# and check the plot. You can leave the folder opened
# to check the plots we will generate in the next exercises.

# Otherwise, you can open it with gpicview:
gpicview HGDP_HWE.png &
```

**Question 5** - Do the genotypic frequencies and allele frequencies seem to follow the Hardy-Weinberg proportions, at least crudely?

**Question 6** - Looking more carefully, is the HW prediction for the proportion of heterozygotes given allele
frequency generally too high or too low relative to the empirically observed values? What might explain the deviation?

Now, let's go through the same steps for the `H938_Euro` set of plink files. 

```bash
plink --bfile ../data/H938_Euro --hardy --chr 2 --out H938_Euro

Rscript ../scripts/e2_2_Euro.R # For the Euro cluster

ls -lh

gpicview HGDP_HWE_Euro.png &
```

**Question 7** - Compare the deficiency in heterozygotes between the world-wide data and the European only data. In which dataset is the deficiency smaller? Why might that be the case? 

## 3 - Allele frequency spectrum

The allele frequency spectrum is a count of the number of variant positions that have a particular
allele frequency count (i.e. the "frequency of different frequecies"!). This is quite simple to do using
the `hist` function in R to make a histogram. The only trick is that there is a variable amount of
missing data in the sample. As a simple way to avoid this issue, we will focus only on SNPs that are
fully observed (i.e. the total counts of individuals = all 938 individuals for the full data problem). 
The R script will take care of this.

### 3.1 Computing and plotting a frequency spectrum of the MAF

```bash
Rscript ../scripts/e3_1.R

gpicview allele_freq_spectrum.png &
```

**Question 8** - The distribution of MAF's does not have the shape you would expect for a constant-sized population. In what ways does it depart from the expectation? 

**Question 9** - What is at least one plausible explanation for the departures? (Hint: This is SNP array data
not sequencing data).

## 4 - Admixture

Though structure within Europe is relatively subtle, we can run the program Admixture on our set of 6
European sub-populations. We will use `K=6` and see if the method can in fact distinguish the 6
sub-populations. The `K` variable is used by admixture as a parameter: it tells
the algorithm in how many clusters it should group the dataset. So here, knowing
in advance that we have individuals from 6 populations, we will use that value.

### 4.1 - Running admixture

```bash
admixture ../data/H938_Euro.LDprune.bed 6
```

As it runs you will see updates describing the progress of the iterative optimization algorithm. For
this data, the program will run for ~100 iterations after the five initial EM steps.

### 4.2 - Plotting the results

When the analysis is finished, you can plot the results in a simple way using the barplot function in R. The script will take care of this.

First, let's check what the data looks like. Notice the output from admixture has
the same prefix as the Plink files, but with the `K` value. The `.Q` file has a
matrix of ancestry estimates for each individual and each of the K clusters:

```bash
less H938_Euro.LDprune.6.Q
# The 6 columns are each of the clusters/ancestries, from K = 6.
# Each row is an individual from the dataset.

# Check the file has data for 124 individuals.
wc -l H938_Euro.LDprune.6.Q
```

```bash
Rscript ../scripts/e4_2_K6.R # Script for K = 6
```

**Question 10** - Are individuals from the population isolates (Adygei, Basque, Orcadian, and Sardian) inferred to have distinct ancestral populations?

**Question 11** - Are the French and Italian individuals completely distinguished as being from distinct populations?

**Question 12** - Which sampled population would seem to have the most internal population structure?

### 4.3 - Follow-up Activities

Now run Admixture with K values of 4 and 5. After the analyses are finished,
you should see new `.4.Q` and `.5.Q` files, which will be used by the following
R scripts:

```bash
Rscript ../scripts/e4_2_K5.R

# ...

Rscript ../scripts/e4_2_K4.R
```

Explore the resulting plots. There are less clusters in which to group the
124 individuals now, so some diverse populations will be confounded in a single
group, or split apart between two groups.

## 5 - PCA

Principal components analysis (PCA) is a commonly used way to investigate population structure in a
sample (though it is also sensitive to close relatedness, batch effects, and long runs of LD, and you
should watch for these potential effects in any analysis). Here you will run PCA on the Euroepan
subset of the data with the LD pruned data.

### 5.1 - Setting up a parameter file and running smartpca

First set-up a basic smartpca parameter file. We will write a new parameters file for `smartpca` using the text editor `gedit`. This file will run smartpca in its most basic mode (i.e. no automatic outlier removal or adjustments for LD - features which you might want to explore later).


```bash
gedit H938_Euro.LDprune.par # Gedit is a plain text editor
```

Fill the contents of the file with these parameters and hit `CTRL+S` to save.
Notice the relative paths to the files. This means that the script will work if
run from the results directory, but not from somewhere else.

```
genotypename: ../data/H938_Euro.LDprune.bed
snpname: ../data/H938_Euro.LDprune.bim
indivname: ../data/H938_Euro.LDprune.PCA.fam
snpweightoutname: ./H938_Euro.LDprune.snpeigs
evecoutname: ./H938_Euro.LDprune.eigs
evaloutname: ./H938_Euro.LDprune.eval
phylipoutname: ./H938_Euro.LDprune.fst
numoutevec: 20
numoutlieriter: 0
outlieroutname: ./H938_Euro.LDprune.out
altnormstyle: NO
missingmode: NO
nsnpldregress: 0
noxdata: YES
nomalexhet: YES
```

Exit the text editor. Now we need to deal with a pesky smartpca issue that will
cause it to ignore individuals in the `.fam` file if they are marked as missing
in the phenotypes column.

```bash
# Ceate new .fam file
awk '{print $1,$2,$3,$4,$5,1}' ../data/H938_Euro.LDprune.fam > ../data/H938_Euro.LDprune.PCA.fam

# Notice that in the smartpca parameters above, the new file ".PCA.fam" is
# the one that we are using.
```

Now run smartpca with the parameters file you created earlier:

```bash
smartpca -p H938_Euro.LDprune.par

# ...

ls -lh
```

You should see the new `.eigs` and `.eval` files created by smartpca. You can
explore the results that will be plotted:

```bash
less -S H938_Euro.LDprune.eigs
```

### 5.2 - Plotting the results

Let's make a plot of the two first principal components: PC1 vs PC2.

```bash
Rscript ../scripts/e5_2_PC1vsPC2.R
```

Make additional plots of PC3 vs PC4, PC5 vs PC6, and PC7 vs PC8 running the rest of the scripts.

```bash
Rscript ../scripts/e5_2_PC3vsPC4.R
Rscript ../scripts/e5_2_PC5vsPC6.R
Rscript ../scripts/e5_2_PC7vsPC8.R
```

Explore the generated plots. Inspect each axis indepedently to understand which individuals
each principal components is distinguishing.

**Question 13** - Are individuals from the population isolates (Adygei, Baseque, Orcadian, and Sardian) clearly separated by at least one of the top PCs you've plotted?

**Question 14** - Are the French and Italian individuals completely separated in at least one of the top PCs
you've plotted?

**Question 15** - Do any of the PCs replicate the structure within Sardinia that was inferred in the admixture analysis above?

**Question 16** - Do the admixture results and PCA results seem to agree with regards to the relationship of the French, Italian, and Orcadian samples?

We have looked at the top 8 PCs, but perhaps we should be looking at more. Plot the proportion
of variance explained by each PC (i.e. the eigenvalues normalized).

```bash
Rscript ../scripts/e5_2_qplot.R
```

Explore the plot in `pca_variance_explained_proportions.png`.

**Question 17** - Based on the proportion of the variation explained - there are a number of PCs that stand out as being more relevant for explaining variation. About how many?

**Question 18** - From your plots of PC1-PC8 you should see that the lower PCs seem to be picking up
individual-level structure, isolating single individuals. At what PC does this first happen?

## 6 - Demonstration of spurious association due to population structure

One consequence of population structure is that it can cause spurious associations with phenotypes.
In this exercise you will generate a phenotype that has no dependence on genetics - but that does
depend on population membership (imagine a trait determined by diet or some other non-genetic
factor that varies among populations). You will try to map it and inspect whether the resulting
association test p-values are consistent with the null of no genetic effects.

### 6.1 - Generate a phenotype 

First, let's generate a file where each individual is assigned a somewhat arbitrary base phenotypic
value given by what population they are from (Adygei = 5, Basque = 2, Italian = 5, Sardinian =
0; French = 8; Orcadian = 10). Notice that `awk` can receive multiple condition + print statement blocks in one line.

```bash
awk '$3 == "Adygei" { print $1, $2, 5 } $3 == "Basque" { print $1, $2, 2 } $3 == "Italian" { print $1, $2, 5 } $3 == "Sardinian" { print $1, $2, 0 } $3 == "French" { print $1, $2, 8 } $3 == "Orcadian" { print $1, $2, 10 }' ../data/Euro.clst.txt > simulated_phenotypes.base.txt


column -t simulated_phenotypes.base.txt
```

Now, using R, let's add some variation around this base value to produce individual-level phenotypes.
Note: Nothing genetic about this phenotype!

```bash
Rscript ../scripts/e6_1.R
```

We will use the generated `simulated_phenotypes.txt` as our phenotype file for mapping.

```bash
column -t simulated_phenotypes.txt
```

### 6.2 - Map the trait using plink mapping functions

The `--assoc` command in plink will produce p-values for a basic regression of phenotype on additive
genotypic score. Explore the generated table after running the command.

```bash
plink --bfile ../data/H938_Euro --pheno simulated_phenotypes.txt --assoc --out H938_Euro_simulated.pheno

less H938_Euro_simulated.pheno.qassoc
```

You can find the p-values of the associations between the SNPs (each one is
identified by a "refseq ID" that is usually called "rs ID" and is used by
NCBI's dbSNP).

Can you use `awk` to find any p-values less than 1 in 10 million in the
last column of the table? Notice that `awk` understands exponent notation,
so you can write `1e-7` instead of a long chain of zeros.

Print the significant p-values alongside the rs ID of the associated SNP. Try
to figure out the command by yourself, and then you can check the solution
below:

```bash
awk '$9 < 1e-7 { print $2, $9 }' H938_Euro_simulated.pheno.qassoc
```

### 6.3 - Exploring the results: A Manhattan plot

Let's read now all the p-values in the `.qassoc` output file and make a Manhattan
plot with them.

```bash
Rscript ../scripts/e6_3.R
```

Explore the resulting `manhattan_plot.png`. The chromosomes have been given
alternate colors to distinguish between them.

**Question 19** - Which chromosomes locations would you be tempted to follow up here?

### 6.4 - Exploring the results: A quantile-quantile plot

Finally, we will generate a plot of the observed vs. expected p-values matched
by quantile.

```bash
Rscript ../scripts/e6_4.R
```

Explore the `q_plot.png`.

**Question 21** - Does there appear to be evidence for a genome-wide inflation of p-values?

