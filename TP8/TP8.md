# Instrucciones para el TP 8

Antes que nada, corra este fix para que anden los ejecutables:

```bash
ln -s ~/repos/tps/TP8 ~/repos/tps/TP8b
ln -s ~/repos/tps/TP6 ~/repos/tps/TP6b
```

Este práctico no transcurre en la línea de comandos de Linux,
sino en un ambiente de ejecución interactiva de código llamado
Jupyter Notebook.

Entre al directorio del TP8 con `cd ~/TP8` e inicie una sesión del server de
Jupyter con: `jupyter notebook TP8.ipynb`.

Una vez que el server comienza a correr, "escucha" el tráfico local
en el puerto 8888 de su propia computadora. Esto significa que, al visitar
la URL http://localhost:8888 debería encontrarse con la interfaz de usuario
de Jupyter.

De todos modos, el TP8 se abrirá automáticamente en su navegador al iniciar
el server. Allí encontrará instrucciones para seguir.

Happy hacking! :keyboard:

